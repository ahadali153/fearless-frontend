# Conference GO!
***
Conference GO! is a conference management app that allows users to create conferences at locations in cities across the US. With the integration of third party API, upon creating a conference at any city, the conference list will display an image of the corresponding city to each conference and we can also get live weather data. Users can sign up to attend conferences, as well as create presentations in conferences.

## Table of Contents
1. [Technologies](#technologies)
2. [Installation](#installation)

## Technologies
***
A list of technologies used within the project:
Python, Javascript, SQL, Django, React, RabbitMQ, microservices, polling, IPC, REST APIs, Docker, Insomnia, Gitlab

## Installation
***
A brief setup to get the microservices up and running. 
```
$ git clone https://gitlab.com/ahadali153/conference-go-react.git
$ cd ../path/to/the/file
$ pip install -r requirements.txt
$ docker compose build
$ docker compose up
$ access react UI with localhost:3000
```

